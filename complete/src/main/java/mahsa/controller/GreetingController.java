package mahsa.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import mahsa.model.Job;
import mahsa.model.Worker;
import mahsa.service.JsonServiceInf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.*;

/**
 * Controller class which is the main body of all project
 * In this class a worker_id will pass through the Rest API and 3 appropriate jobs will return
 */

@RestController
public class GreetingController {


    @Autowired
    private JsonServiceInf jsonService;

    @RequestMapping("/api/getjob") //use this for URL Example: localhost:8000/api/getjob
    /**
     * Example URL: localhost:8000/api/getjob
     * default value 5 was the best for all the tests
     * If one wants to change this, they should use post to send another worker_id
     */
    public ArrayList<Job> greeting(@RequestParam(value = "worker_id", defaultValue = "5") int worker_id) {

        ArrayList<Job> result = new ArrayList<Job>();
        ArrayList<Job> finalResult = new ArrayList<Job>();
        if (worker_id != -1) {
            try {
                //getting Arrays with google json
                Worker[] workers = jsonService.getWorkers("http://test.swipejobs.com/api/workers");
                Job[] jobs = jsonService.getJobs("http://test.swipejobs.com/api/jobs");

                Gson gson = new GsonBuilder().serializeNulls().create();

                Worker selectWorker = workers[worker_id];
                //Worker must be Active
                if (selectWorker.isActive() == true) {
                    for (int j = 0; j < jobs.length; j++) {
                        //check matching with requireCertificates
                        Boolean match = jsonService.matchWorker(selectWorker, jobs[j]);
                        if (match == true) {
                            //check the DriverLicenseequirment
                            if ((jobs[j].getDriverLicenseRequired() == true && selectWorker.isHasDriversLicense() == true) || (jobs[j].getDriverLicenseRequired() == false)) {
                                //Checking location of job and worker and match them with maxDistance of Worker
                                double calDistance = jsonService.calculateDistance(jobs[j].getLocation().getLatitude(), jobs[j].getLocation().getLongitude(), selectWorker.getJobSearchAddress().getLatitude(), selectWorker.getJobSearchAddress().getLongitude());
                                if (calDistance <= selectWorker.getJobSearchAddress().getMaxJobDistance())
                                    result.add(jobs[j]);
                            }
                        }
                    }
                    finalResult = jsonService.sortBySalary(result);

                    return finalResult;
                } else {
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //returning result
        return finalResult;
    }
}
