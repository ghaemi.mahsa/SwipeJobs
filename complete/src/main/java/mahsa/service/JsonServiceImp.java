package mahsa.service;

import com.google.gson.Gson;
import mahsa.model.Job;
import mahsa.model.Worker;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * All the Services are in this class
 */

@Service
public class JsonServiceImp implements JsonServiceInf {
    private final Gson GSON = new Gson();

    //reading Gson File with the URL
    private String getJson(String urlToRead) throws Exception {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        return result.toString();
    }

    @Override
    public Worker[] getWorkers(String urlToRead) throws Exception {
        String json_txt_workers = getJson(urlToRead);
        return GSON.fromJson(json_txt_workers, Worker[].class);
    }

    @Override
    public Job[] getJobs(String urlToRead) throws Exception {
        String json_txt_jobs = getJson(urlToRead);
        return GSON.fromJson(json_txt_jobs, Job[].class);
    }

    //match with certificates
    @Override
    public Boolean matchWorker(Worker selectWorker, Job job) {
        Boolean result = false;
        ArrayList<String> certificate = selectWorker.getCertificates();
        ArrayList<String> requireCertificate = job.getRequiredCertificates();

        if (certificate.containsAll(requireCertificate)) {
            result = true;
        }

        return result;
    }

    //calculating the distance of workers and job locations
    @Override
    public double calculateDistance(double lat1, double lon1, double lat2, double lon2) {
        double distance = 0;
        int RADIUS = 6371; // average radius of the earth in km
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        distance = RADIUS * c;
        return distance;
    }

    //Sorting array with BillRates to find the best 3 jobs
    @Override
    public ArrayList sortBySalary(ArrayList<Job> result) {
        List<Float> bill = new ArrayList<Float>();
        for (int i = 0; i < result.size(); i++)
            bill.add(Float.valueOf(result.get(i).getBillRate().substring(1)));

        Collections.sort(bill, Collections.reverseOrder());
        ArrayList<Job> finalResult = new ArrayList<Job>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < result.size(); j++) {
                if ((Float.valueOf(result.get(j).getBillRate().substring(1)).compareTo(bill.get(i))) == 0) {
                    finalResult.add(result.get(j));
                }
            }
        }
        return finalResult;
    }
}
