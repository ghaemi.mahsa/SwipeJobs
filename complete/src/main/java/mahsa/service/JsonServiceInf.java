package mahsa.service;


import mahsa.model.Job;
import mahsa.model.Worker;

import java.util.ArrayList;

/**
 * using Interface Class for hiding details from client
 */
public interface JsonServiceInf {

    double calculateDistance(double lat1, double lon1, double lat2, double lon2);

    Worker[] getWorkers(String urlToRead)  throws Exception;
    Job[] getJobs(String urlToRead)  throws Exception;

    Boolean matchWorker(Worker selectWorker, Job job);

    ArrayList sortBySalary(ArrayList<Job> result);
}
