package mahsa.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Job class
 */
public class Job {

    private boolean driverLicenseRequired;
    private String about;
    private String jobTitle;
    private String company;
    private String guid;
    private int jobId;
    private ArrayList<String> requiredCertificates;
    private Location location;
    private String billRate;
    private int workersRequired;
    private Date startDate;

    public boolean getDriverLicenseRequired() {
        return this.driverLicenseRequired;
    }

    public void setDriverLicenseRequired(boolean driverLicenseRequired) {
        this.driverLicenseRequired = driverLicenseRequired;
    }

    public ArrayList<String> getRequiredCertificates() {
        return requiredCertificates;
    }

    public void setRequiredCertificates(ArrayList<String> requiredCertificates) {
        this.requiredCertificates = requiredCertificates;
    }

    public Location getLocation() {
        return this.location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getBillRate() {
        return this.billRate;
    }

    public void setBillRate(String billRate) {
        this.billRate = billRate;
    }

    public int getWorkersRequired() {
        return this.workersRequired;
    }

    public void setWorkersRequired(int workersRequired) {
        this.workersRequired = workersRequired;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getAbout() {
        return this.about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getJobTitle() {
        return this.jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getCompany() {
        return this.company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getGuid() {
        return this.guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public int getJobId() {
        return this.jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }
}
